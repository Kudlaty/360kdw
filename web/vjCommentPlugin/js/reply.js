$(document).ready(function() {
    $('.artykul_social .button_comment').click(function() {
        $('.form-comment').slideToggle(1000);
    });

    $('.form-comment form').submit(function() {
        var blad = false;
        var author_name = $('#comment_author_name').val().trim();
        author_name = author_name.replace(new RegExp('\\s+', 'g'), '');
        var author_email = $('#comment_author_email').val().trim();
        var author_www = $('#comment_author_www').val().trim();
        author_www = author_www.replace('http://', '');
        var comment_body = $('#comment_body').val().trim();
        comment_body = comment_body.replace(new RegExp('\\s+', 'g'), '');

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var wwwReg = /^([\w-\.]+\.[\w-]{2,4})?$/;

        if(author_name == "") {
            blad = true;
            $('.author_name_error').show();
        }
        if ( author_email == "" || !emailReg.test(author_email) ) {
            blad = true;
            $('.author_email_error').show();
        }
        if ( author_www != "" && !wwwReg.test(author_www) ) {
            blad = true;
            $('.author_www_error').show();
        }
        if ( author_www != "" && wwwReg.test(author_www) ) {
            $('#comment_author_www').val('http://' + author_www);
        }
        if ( comment_body.length < 10 ) {
            blad = true;
            $('.body_error').show();
        }

        if(blad) {
            $('.error_wrap').each(function() {
                var height = $(this).innerHeight();
                $(this).animate({ opacity:1 },5000).animate({opacity: 0, height: 0}, 1000, 'linear', function() { $(this).hide().css({height: height, opacity: 1}) });
            });
            return false;
        }
    });
});

function reply(id, value, form_name){
    document.getElementById(form_name+"_reply").value = id;
    document.getElementById(form_name+"_reply_author").value = value;
    document.getElementById("reply_author_"+form_name).style.display = "block";
    document.getElementById("reply_author_delete_"+form_name).style.display = "inline";
    document.location.href = document.location.toString().split("#")[0]+"#comments-"+form_name;
}

function deleteReply(form_name){
    document.getElementById(form_name+"_reply").value = "";
    document.getElementById(form_name+"_reply_author").value = "";
    document.getElementById("reply_author_"+form_name).style.display = "none";
    document.getElementById("reply_author_delete_"+form_name).style.display = "none";
}
