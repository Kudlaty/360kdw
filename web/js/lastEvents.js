$(document).ready(function() {
    $('.event_box .list').click(function() {
        $('.event_box .details:visible').each(function() {
            $(this).slideToggle().prev('.list').slideToggle();
        });
        var list_box = $(this);
        $(this).next('.details').slideToggle(500, function() { list_box.slideToggle(); } );
    });
    $('.event_box .details').click(function() {
        var det_box = $(this);
        $(this).prev('.list').slideToggle(500, function() { det_box.slideToggle(); } );
    });
});
