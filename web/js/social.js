$(document).ready(function() {
    // Fade in images so there isn't a color "pop" document load and then on window load
    $(".greyScale").fadeIn(100);

    $('.greyScale').css({"position":"absolute"});
    $('.img_grayscale').css({"opacity":"0","z-index":"998"});

    // Fade image
    $('.greyScale').mouseover(function(){
        $(this).parent().find('img:first').stop().animate({opacity:1}, 10);
    });

    $('.img_grayscale').mouseout(function(){
        $(this).stop().animate({opacity:0}, 1000);
    });
});
