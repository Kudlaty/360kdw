$(document).ready(function() {
    // Fade in images so there isn't a color "pop" document load and then on window load
    $(".greyScale").fadeIn(100);
    $('.main_photo > div:gt(0)').hide();
    $('.main_photo').css({ height: $('.main_photo div:first-child').outerHeight(true) });

    $('.greyScale').css({"position":"absolute"});
    $('.img_grayscale').css({"opacity":"0","z-index":"998"});

    // Fade image
    $('.greyScale').mouseover(function(){
        $(this).parent().find('img:first').stop().animate({opacity:1}, 10);
    });

    $('.img_grayscale').mouseout(function(){
        $(this).stop().animate({opacity:0}, 1000);
    });

    $('.galeria_arrow_l').click(function() {
        previous = $('.main_photo .photo:visible').prev();

        if(previous.length == 0) {
            previous = $('.main_photo .photo:last-child');
        }

        show_next(previous);

        return false;
    });

    $('.galeria_arrow_r').click(function() {
        var next = $('.main_photo .photo:visible').next();

        if(next.length == 0) {
            next = $('.main_photo .photo:first-child');
        }

        show_next(next);

        return false;
    });

    $('.greyScale').click(function() {
        show_next($(this));

        return false;
    });
});

function show_next(data) {
//     $('.main_photo div:first-child').after($('.main_photo div[data=' + data.attr('data') + ']'));
    $('.main_photo .photo:visible').fadeOut(1000).parent().children('.photo[data=' + data.attr('data') + ']').fadeIn(1000).end();
    $('.main_photo').animate({ height: $('.main_photo .photo[data=' + data.attr('data') + ']').outerHeight(true) }, 1000);

    return false;
}
