$(document).ready(function() {
    var change_name = false;

    if(!change_name) {
        $("#sf_fieldset_none #gallery_author_id").attr('name', $("#sf_fieldset_none #gallery_author_id").attr('name') + '[]');
        change_name = true;
    }

    $('#select_images #images_to_upload').change(function() {
        var files = this.files;
        for( x in files ) {
            if(typeof files[x] == 'object') {
                var fr = new FileReader();
                fr.file = files[x];
                fr.onloadend = showFileInList;
                fr.readAsDataURL(files[x]);
            }
        }
    });

    $('form').submit(function() {
        uploadQueue();

        return false;
    });
});

var formList = new Object();

var showFileInList = function (ev) {
    var file = ev.target.file;
    if (file && (typeof formList[file.name] == "undefined") ) {
        var fieldset = $('#sf_fieldset_none').clone().appendTo($('#my_new_images'));
        fieldset.css({width: 450, 'margin-top': 30, display: 'inline-block'}).show();
        fieldset.find('.usun').bind('click', function() {
            delete formList[file.name];
            fieldset.slideToggle(1000, function() { $(this).remove(); });

            return false;
        });
        if (file.type.search(/image\/.*/) != -1) {
            var thumb = new Image();
            thumb.src = ev.target.result;
            thumb.addEventListener("mouseover", showImagePreview, false);
            thumb.addEventListener("mouseout", removePreview, false);
            fieldset.find('.gallery_image').prepend(thumb);
            fieldset.find('#gallery_name').val(file.name);
        }

        formList[file.name] = {
            file: file,
            fieldset: fieldset,
            file_src: ev.target.result
        };
    }
}
var preview = null;

var showImagePreview = function (ev) {
    var div = document.createElement("div");
    div.style["top"] = (ev.pageY + 10) + "px";
    div.style["left"] = (ev.pageX + 10) + "px";
    div.style["opacity"] = 0;
    div.className = "imagePreview";
    var img = new Image();
    img.src = ev.target.src;
    div.appendChild(img);
    document.body.appendChild(div);
    document.body.addEventListener("mousemove", movePreview, false);
    preview = div;
    fadePreviewIn();
}

var movePreview = function (ev) {
    if (preview) {
        preview.style["top"] = (ev.pageY + 10) + "px";
        preview.style["left"] = (ev.pageX + 10) + "px";
    }
}

var removePreview = function (ev) {
    document.body.removeEventListener("mousemove", movePreview, false);
    document.body.removeChild(preview);
}

var fadePreviewIn = function () {
    if (preview) {
        var opacity = preview.style["opacity"];
        for (var i = 10; i < 250; i = i+10) {
            (function () {
                var level = i;
                setTimeout(function () {
                    preview.style["opacity"] = opacity + level / 250;
                }, level);
            })();
        }
    }
}

var uploadQueue = function () {
    for( key in formList ) {
        var item = formList[key];
        $('<div class="loader">Uploading...</div>').appendTo(item.fieldset);

        uploadFile(item.file, item.fieldset, item.file_src);
        delete formList[key];
    }
}

var uploadFile = function (file, fieldset, src) {
    if (fieldset && file) {
        var xhr = new XMLHttpRequest(),
            upload = xhr.upload;
        upload.addEventListener("progress", function (ev) {
            if (ev.lengthComputable) {
                var loader = fieldset.find(".loader");
                loader.css({width: (ev.loaded / ev.total) * 100 + "%"});
                loader.text("Uploading: "+ Math.round((ev.loaded / ev.total) * 100) + "%");
            }
        }, false);
        upload.addEventListener("load", function (ev) {
            var div = fieldset.find(".loader");
            div.css({width: "100%", "background-color": "#0f0", color: "#3DD13F"});
            div.text("Upload complete");
            fieldset.slideToggle(1000, function(){ $(this).remove() });
        }, false);
        upload.addEventListener("error", function (ev) {console.log(ev);}, false);
        xhr.open(
            "POST",
            $("form").attr('action')
        );
        xhr.setRequestHeader("Cache-Control", "no-cache");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.setRequestHeader("X-File-Name", file.name);
        var all = {
            "file" : src,
            "file_name": file.name,
            "name" : fieldset.find('#gallery_name').val(),
            "desc" : fieldset.find('#gallery_description').val(),
            "autor" : fieldset.find('#gallery_author_id').val(),
            "status" : fieldset.find('#gallery_status').val(),
            "id" : $('#gallery_id').val(),
            "csrf" : $('#gallery__csrf_token').val()
        };

        xhr.send(JSON.stringify(all, null, 4));
    }
}
