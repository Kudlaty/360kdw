$(document).ready(function() {
    $('#news_description').editor({
        autoEnable: true, // Enable the editor automaticly
        disabledUi: ['cancel', 'save'],
        disabledPlugins: ['unsavedEditWarning'],
        unloadWarning: false,
        plugins: { // Plugin options
            dock: { // Dock specific plugin options
                docked: true, // Start the editor already docked
                dockToElement: true, // Dock the editor inplace of the element
                persist: false // Do not save the docked state
            }
        }
    });
});
