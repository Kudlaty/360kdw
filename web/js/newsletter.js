$(document).ready(function() {
    $('#newsletter_form').submit(function() {
        var blad = false;
        var newsletter = $('.form_newsletter').val();
        newsletter = newsletter.replace(new RegExp('\\s+', 'g'), '');

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if ( newsletter == "" || !emailReg.test(newsletter) ) {
            blad = true;
            $('.newsletter_email_error').show();
            var height = $('.newsletter_email_error').innerHeight();
            $('.newsletter_email_error').css({ bottom: height });
            $('.newsletter_email_error').animate({ opacity:1 },5000).animate({opacity: 0, height: 0}, 1000, 'linear', function() { $(this).hide().css({height: height, opacity: 1}) });
        }

        if(blad) {
            return false;
        }
    });
});
