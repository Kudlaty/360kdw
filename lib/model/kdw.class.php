<?php

class kdw {
    static public function slugify($text) {
        $text = self::normalize($text);

        $text = preg_replace('/\W+/', '-', $text);

        $text = strtolower(trim($text, '-'));

        return $text;
    }

    static public function normalize($string) {
        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $pol = array("ą", "ć", "ę", "ł", "ń", "ó", "ś", "ź", "ż", "Ą", "Ć", "Ę", "Ł", "Ń", "Ó", "Ś", "Ź", "Ż", "„", "”");
        $uni = array("a", "c", "e", "l", "n", "o", "s", "z", "z", "A", "C", "E", "L", "N", "O", "S", "Z", "Z", '"', '"');

        $string = str_replace($pol, $uni, $string);

        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        $string = strtolower($string);
        return utf8_encode($string);
    }

    static public function resize($file, $filename, $height, $width, $thumb_dir, $aspect, $resize, $thumb_gray_dir = Null) {
        if($aspect) {
            $aspect = True;
        } else {
            $aspect = False;
        }

        if($resize) {
            $resize = True;
        } else {
            $resize = False;
        }

        list($resource_x, $resource_y) = getimagesize($filename);

        if($thumb_gray_dir) {
            $grayscale = new sfImage($filename);
            $grayscale->greyscale();
            mkdir($thumb_gray_dir, 0777, true);

            if($resize) {
                $aspect = false;
                $grayscale->crop(($resource_x - $width)/2, ($resource_y - $height)/2, $width,$height);
            } else {

                if($height && $width) {
                    $grayscale->resize($width,$height, false, $aspect);
                } elseif(!$height && $width) {
                    $grayscale->resize($width,null, false, $aspect);
                } elseif($height && !$width) {
                    $grayscale->resize(null,$height, false, $aspect);
                }

            }

            $grayscale->saveAs($thumb_gray_dir.DIRECTORY_SEPARATOR.$file);
        }

        $thumbnail = new sfImage($filename);

        if($resize) {
            $aspect = false;
            $thumbnail->crop(($resource_x - $width)/2, ($resource_y - $height)/2, $width,$height);
        } else {

            if($height && $width) {
                $thumbnail->resize($width,$height, false, $aspect);
            } elseif(!$height && $width) {
                $thumbnail->resize($width,null, false, $aspect);
            } elseif($height && !$width) {
                $thumbnail->resize(null,$height, false, $aspect);
            }

        }
//         list($overlay_x, $overlay_y) = getimagesize('images/watermark.png');
//         $thumbnail->overlay(new sfImage('images/watermark.png'), array($resource_x - $overlay_x - 10, $resource_y - $overlay_y - 10));

        mkdir($thumb_dir, 0777, true);
        $thumbnail->saveAs($thumb_dir.DIRECTORY_SEPARATOR.$file);

        return $thumbnail;
    }
}