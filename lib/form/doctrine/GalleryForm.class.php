<?php

/**
 * Gallery form.
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GalleryForm extends BaseGalleryForm
{
    public function configure()
    {
        $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array(
            'file_src'    => '/uploads/gallery/img/'.$this->getObject()->image,
            'edit_mode'   => !$this->isNew(),
            'label' => 'Obraz',
            'is_image'    => true,
            'with_delete' => true,
            'template'    => '<div class="input_file"><a target="_blank" href="/uploads/gallery/img/'.$this->getObject()->image.'">%file%</a></div><br />%input%<br />%delete% %delete_label%'
        ), array(
            'multiple'  => 'multiple',
        ));

        $this->validatorSchema['image'] = new sfValidatorFile(array(
            'required'   => false,
            'path'       => sfConfig::get('sf_upload_dir').'/gallery/img',
            'mime_types' => 'web_images',
        ));

        $this->validatorSchema['image_delete'] = new sfValidatorPass();
    }
}
