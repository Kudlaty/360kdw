<?php

/**
 * Config form.
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ConfigForm extends BaseConfigForm
{
    public function configure()
    {
        $this->widgetSchema->setHelps(array(
            'email_new_news_body' => 'W treści można umieścić:<br />
                    <strong>{NEWS_LINK}</strong> - zostanie podmieniony na link do dodawanego newsa<br />
                    <strong>{NEWS_LEAD}</strong> - zostanie zmieniony na wstęp dodawanego newsa<br />
                    <strong>{NEWS_NAME}</strong> - zostanie zmieniony na tytuł dodawanego newsa',
            'email_new_news_from' => 'Email będzie ustawiony w nagłówku od:',
            'email_new_news_subject' => 'W treści można umieścić:<br />
                    <strong>{NEWS_LINK}</strong> - zostanie podmieniony na link do dodawanego newsa<br />
                    <strong>{NEWS_LEAD}</strong> - zostanie zmieniony na wstęp dodawanego newsa<br />
                    <strong>{NEWS_NAME}</strong> - zostanie zmieniony na tytuł dodawanego newsa',
            'send_mails_on_add_news' => 'Włącza lub wyłącza wysyłanie maili po dodaniu nowego newsa',
        ));

        $this->widgetSchema->setLabels(array(
            'email_new_news_body' => 'Treść maila',
            'email_new_news_from' => 'Od',
            'email_new_news_subject' => 'Temat maila',
            'send_mails_on_add_news' => 'On / Off',
        ));
    }
}
