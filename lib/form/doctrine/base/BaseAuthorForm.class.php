<?php

/**
 * Author form base class.
 *
 * @method Author getObject() Returns the current form's model object
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAuthorForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'author'    => new sfWidgetFormInputCheckbox(),
      'photo'     => new sfWidgetFormInputText(),
      'name'      => new sfWidgetFormInputText(),
      'team'      => new sfWidgetFormInputText(),
      'www'       => new sfWidgetFormInputText(),
      'gg'        => new sfWidgetFormInputText(),
      'jabber'    => new sfWidgetFormInputText(),
      'skype'     => new sfWidgetFormInputText(),
      'email'     => new sfWidgetFormInputText(),
      'phone'     => new sfWidgetFormInputText(),
      'join_date' => new sfWidgetFormDate(),
      'status'    => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'author'    => new sfValidatorBoolean(array('required' => false)),
      'photo'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name'      => new sfValidatorString(array('max_length' => 255)),
      'team'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'www'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'gg'        => new sfValidatorInteger(array('required' => false)),
      'jabber'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'skype'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'phone'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'join_date' => new sfValidatorDate(array('required' => false)),
      'status'    => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('author[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Author';
  }

}
