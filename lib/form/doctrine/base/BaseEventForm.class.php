<?php

/**
 * Event form base class.
 *
 * @method Event getObject() Returns the current form's model object
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEventForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'name'        => new sfWidgetFormInputText(),
      'topic'       => new sfWidgetFormInputText(),
      'responsible' => new sfWidgetFormInputText(),
      'where'       => new sfWidgetFormInputText(),
      'event_date'  => new sfWidgetFormDate(),
      'event_time'  => new sfWidgetFormTime(),
      'status'      => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255)),
      'topic'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'responsible' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'where'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'event_date'  => new sfValidatorDate(),
      'event_time'  => new sfValidatorTime(array('required' => false)),
      'status'      => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('event[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Event';
  }

}
