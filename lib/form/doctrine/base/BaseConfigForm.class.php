<?php

/**
 * Config form base class.
 *
 * @method Config getObject() Returns the current form's model object
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseConfigForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'send_mails_on_add_news' => new sfWidgetFormInputCheckbox(),
      'email_new_news_subject' => new sfWidgetFormInputText(),
      'email_new_news_from'    => new sfWidgetFormInputText(),
      'email_new_news_body'    => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'send_mails_on_add_news' => new sfValidatorBoolean(array('required' => false)),
      'email_new_news_subject' => new sfValidatorString(array('max_length' => 255)),
      'email_new_news_from'    => new sfValidatorString(array('max_length' => 255)),
      'email_new_news_body'    => new sfValidatorString(array('max_length' => 16777215)),
    ));

    $this->widgetSchema->setNameFormat('config[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Config';
  }

}
