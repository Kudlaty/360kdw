<?php

/**
 * Event form.
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EventForm extends BaseEventForm
{
    public function configure()
    {
        $this->widgetSchema['event_date'] = new sfWidgetFormJQueryDate(array(
            'config' => '{
                changeMonth: true,
                changeYear: true,
                showAnim: "drop",
                monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
                monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"]
            }',
        ));

        $minutes = array();
        $minutes[""] = "";
        for($i = 0; $i <= 60; $i += 5) {
            $minutes[] = $i;
        }
        $this->widgetSchema['event_time'] = new sfWidgetFormTime(array(
            'can_be_empty' => true,
            'minutes' => array_combine($minutes, $minutes),
        ));

        $this->widgetSchema->setHelps(array(
            'topic' => 'Jeśli nie zostanie uzupełniony, to tematem będzie nazwa',
            'event_time' => 'Nie jest wymagana',
        ));

        $this->widgetSchema->setLabels(array(
            'name' => 'Nazwa',
            'topic' => 'Temat',
            'responsible' => 'Odpowiedzialny',
            'where' => 'Gdzie',
            'event_date' => 'Data',
            'event_time' => 'Godzina',
        ));
    }
}
