<?php

/**
 * NewsletterMail form.
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewsletterMailForm extends BaseNewsletterMailForm
{
    public function configure()
    {
        $this->disableLocalCSRFProtection();
    }
}
