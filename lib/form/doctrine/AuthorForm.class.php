<?php

/**
 * Author form.
 *
 * @package    360kdw
 * @subpackage form
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AuthorForm extends BaseAuthorForm
{
  public function configure()
  {
        $this->widgetSchema['photo'] = new sfWidgetFormInputFileEditable(array(
            'file_src'    => '/uploads/author/img/'.$this->getObject()->photo,
            'edit_mode'   => !$this->isNew(),
            'label' => 'Zdjęcie',
            'is_image'    => true,
            'with_delete' => true,
            'template'    => '<div class="input_file"><a target="_blank" href="/uploads/author/img/'.$this->getObject()->photo.'">%file%</a></div><br />%input%<br />%delete% %delete_label%'
        ));

        $this->validatorSchema['photo'] = new sfValidatorFile(array(
            'required'   => false,
            'path'       => sfConfig::get('sf_upload_dir').'/author/img',
            'mime_types' => 'web_images',
        ));

        $this->validatorSchema['photo_delete'] = new sfValidatorPass();

        $this->widgetSchema['join_date'] = new sfWidgetFormJQueryDate(array(
            'config' => '{
                changeMonth: true,
                changeYear: true,
                showAnim: "drop",
                monthNamesShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
                monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"]
            }',
        ));

        $this->widgetSchema->setHelps(array(
            'author' => 'Jeśli zaznaczone, to będzie się wyświetlać, także jako autor zdjęć.',
        ));
        $this->widgetSchema->setLabels(array(
            'author' => 'Autor',
            'name' => 'Nazwa',
            'team' => 'Drużyna',
            'gg' => 'GG',
            'www' => 'www',
            'phone' => 'Telefon',
            'join_date' => 'Data dołączenia do drużyny',
        ));
  }
}
