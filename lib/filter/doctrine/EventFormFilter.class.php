<?php

/**
 * Event filter form.
 *
 * @package    360kdw
 * @subpackage filter
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EventFormFilter extends BaseEventFormFilter
{
  public function configure()
  {
  }
}
