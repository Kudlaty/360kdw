<?php

/**
 * NewsletterMail filter form base class.
 *
 * @package    360kdw
 * @subpackage filter
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseNewsletterMailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'email'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'email'  => new sfValidatorPass(array('required' => false)),
      'status' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('newsletter_mail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NewsletterMail';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'email'  => 'Text',
      'status' => 'Boolean',
    );
  }
}
