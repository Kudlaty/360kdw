<?php

/**
 * Author filter form base class.
 *
 * @package    360kdw
 * @subpackage filter
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAuthorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'author'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'photo'     => new sfWidgetFormFilterInput(),
      'name'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'team'      => new sfWidgetFormFilterInput(),
      'www'       => new sfWidgetFormFilterInput(),
      'gg'        => new sfWidgetFormFilterInput(),
      'jabber'    => new sfWidgetFormFilterInput(),
      'skype'     => new sfWidgetFormFilterInput(),
      'email'     => new sfWidgetFormFilterInput(),
      'phone'     => new sfWidgetFormFilterInput(),
      'join_date' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'status'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'author'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'photo'     => new sfValidatorPass(array('required' => false)),
      'name'      => new sfValidatorPass(array('required' => false)),
      'team'      => new sfValidatorPass(array('required' => false)),
      'www'       => new sfValidatorPass(array('required' => false)),
      'gg'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'jabber'    => new sfValidatorPass(array('required' => false)),
      'skype'     => new sfValidatorPass(array('required' => false)),
      'email'     => new sfValidatorPass(array('required' => false)),
      'phone'     => new sfValidatorPass(array('required' => false)),
      'join_date' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'status'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('author_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Author';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'author'    => 'Boolean',
      'photo'     => 'Text',
      'name'      => 'Text',
      'team'      => 'Text',
      'www'       => 'Text',
      'gg'        => 'Number',
      'jabber'    => 'Text',
      'skype'     => 'Text',
      'email'     => 'Text',
      'phone'     => 'Text',
      'join_date' => 'Date',
      'status'    => 'Boolean',
    );
  }
}
