<?php

/**
 * Event filter form base class.
 *
 * @package    360kdw
 * @subpackage filter
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseEventFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'topic'       => new sfWidgetFormFilterInput(),
      'responsible' => new sfWidgetFormFilterInput(),
      'where'       => new sfWidgetFormFilterInput(),
      'event_date'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'event_time'  => new sfWidgetFormFilterInput(),
      'status'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'name'        => new sfValidatorPass(array('required' => false)),
      'topic'       => new sfValidatorPass(array('required' => false)),
      'responsible' => new sfValidatorPass(array('required' => false)),
      'where'       => new sfValidatorPass(array('required' => false)),
      'event_date'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'event_time'  => new sfValidatorPass(array('required' => false)),
      'status'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('event_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Event';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'name'        => 'Text',
      'topic'       => 'Text',
      'responsible' => 'Text',
      'where'       => 'Text',
      'event_date'  => 'Date',
      'event_time'  => 'Text',
      'status'      => 'Boolean',
    );
  }
}
