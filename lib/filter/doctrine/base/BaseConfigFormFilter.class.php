<?php

/**
 * Config filter form base class.
 *
 * @package    360kdw
 * @subpackage filter
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseConfigFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'send_mails_on_add_news' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'email_new_news_subject' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email_new_news_from'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email_new_news_body'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'send_mails_on_add_news' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'email_new_news_subject' => new sfValidatorPass(array('required' => false)),
      'email_new_news_from'    => new sfValidatorPass(array('required' => false)),
      'email_new_news_body'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('config_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Config';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'send_mails_on_add_news' => 'Boolean',
      'email_new_news_subject' => 'Text',
      'email_new_news_from'    => 'Text',
      'email_new_news_body'    => 'Text',
    );
  }
}
