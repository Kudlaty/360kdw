<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
</head>
<body>
    <?php if ($sf_user->isAuthenticated()): ?>
    <div>
        <ul class="main_menu">
        <li>
            <a href="<?php echo url_for('@news') ?>">News</a>
        </li>
        <li>
            <a href="<?php echo url_for('@event') ?>">Wydarzenia</a>
        </li>
        <li>
            <a href="<?php echo url_for('@gallery') ?>">Galeria</a>
        </li>
        <li>
            <a href="<?php echo url_for('@author') ?>">Ludzie/Autorzy</a>
        </li>
        <li>
            <a href="<?php echo url_for('@newsletter_mail') ?>">Newsletter</a>
        </li>
        <li>
            <a href="<?php echo url_for('@commentAdmin') ?>">Komentarze</a>
        </li>
        <li>
            <a href="<?php echo url_for('@commentReportAdmin') ?>">Raporty komentarzy</a>
        </li>
        <li>
            <a href="<?php echo url_for('@sf_guard_user') ?>">Użytkownicy</a>
        </li>
        <li>
            <a href="<?php echo url_for('@config_edit?id=1') ?>">Konfiguracja</a>
        </li>
        <li>
            <a href="<?php echo url_for('@sf_guard_signout') ?>">Wyloguj</a>
        </li>
        </ul>
    </div>
    <?php endif; ?>

    <?php echo $sf_content ?>
</body>
</html>
