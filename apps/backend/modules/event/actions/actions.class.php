<?php

require_once dirname(__FILE__).'/../lib/eventGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/eventGeneratorHelper.class.php';

/**
 * event actions.
 *
 * @package    360kdw
 * @subpackage event
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class eventActions extends autoEventActions
{
    public function executeBatchChange_status(sfWebRequest $request)
    {
        $ids = $request->getParameter('ids');

        $q = Doctrine_Query::create()
        ->from('Event n')
        ->whereIn('n.id', $ids);

        foreach ($q->execute() as $item)
        {
            if($item->getStatus()) {
                $item->setStatus(false);
            } else {
                $item->setStatus(true);
            }
            $item->save();
        }

        $this->getUser()->setFlash('notice', "Zaznaczone wydarzenia, zmieniły status");

        $this->redirect('event');
    }
}
