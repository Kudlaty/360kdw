<?php

/**
 * newsletter_mail module configuration.
 *
 * @package    360kdw
 * @subpackage newsletter_mail
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsletter_mailGeneratorConfiguration extends BaseNewsletter_mailGeneratorConfiguration
{
}
