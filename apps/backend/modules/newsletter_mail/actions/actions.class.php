<?php

require_once dirname(__FILE__).'/../lib/newsletter_mailGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/newsletter_mailGeneratorHelper.class.php';

/**
 * newsletter_mail actions.
 *
 * @package    360kdw
 * @subpackage newsletter_mail
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsletter_mailActions extends autoNewsletter_mailActions
{
    public function executeBatchChange_status(sfWebRequest $request)
    {
        $ids = $request->getParameter('ids');

        $q = Doctrine_Query::create()
        ->from('NewsletterMail n')
        ->whereIn('n.id', $ids);

        foreach ($q->execute() as $item)
        {
            if($item->getStatus()) {
                $item->setStatus(false);
            } else {
                $item->setStatus(true);
            }
            $item->save();
        }

        $this->getUser()->setFlash('notice', "Zaznaczone maile, zmieniły status");

        $this->redirect('newsletter_mail');
    }
}
