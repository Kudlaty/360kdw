<?php

require_once dirname(__FILE__).'/../lib/galleryGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/galleryGeneratorHelper.class.php';

/**
 * gallery actions.
 *
 * @package    360kdw
 * @subpackage gallery
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class galleryActions extends autoGalleryActions
{
    public function executeBatchChange_status(sfWebRequest $request)
    {
        $ids = $request->getParameter('ids');

        $q = Doctrine_Query::create()
        ->from('Gallery n')
        ->whereIn('n.id', $ids);

        foreach ($q->execute() as $item)
        {
            if($item->getStatus()) {
                $item->setStatus(false);
            } else {
                $item->setStatus(true);
            }
            $item->save();
        }

        $this->getUser()->setFlash('notice', "Zaznaczone zdjęcia, zmieniły status");

        $this->redirect('gallery');
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->form = $this->configuration->getForm();
        $this->gallery = $this->form->getObject();

        $this->preProcessForm($request, $this->form);

        $this->setTemplate('new');
    }

    protected function preProcessForm(sfWebRequest $request, sfForm $form)
    {
        $post_params = json_decode(file_get_contents("php://input"), true);
        $data = explode(',', $post_params['file']);
        $file_src = base64_decode($data[1]);
        $mime = substr($data[0], 5, strrpos($data[0], ';') - 5);

        $tmp_name = "/tmp/".substr(md5(time()), 5);
        file_put_contents($tmp_name, $file_src);

        $new_p = array();
        $new_f = array();


        $file = array(
            "error" => 0,
            "name" => $post_params['file_name'],
            "type" => $mime,
            "tmp_name" => $tmp_name,
            "size" => filesize($tmp_name)
        );

        $new_form = new GalleryForm();
        $new_p['id'] = $post_params['id'];
        $new_p['_csrf_token'] = $post_params['csrf'];
        $new_p['name'] = $post_params['name'];
        $new_p['description'] = $post_params['desc'];
        $new_p['author_id'] = $post_params['autor'];
        $new_p['status'] = $post_params['status'];
        $new_f['image'] = $file;

//     print_r(array($post_params, $new_p, $new_f)); exit;

        $this->processNewForm($new_p, $new_f, $request, $new_form);

        if ($request->hasParameter('_save_and_add')) {
            $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

            $this->redirect('@gallery_new');
        } else {
            $this->getUser()->setFlash('notice', $notice);

            $this->redirect('@gallery');
        }
    }

    protected function processNewForm($parameters, $files, $request, $form)
    {
        $form->bind($parameters, $files);
        if ($form->isValid()) {
            $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

            try {
                $gallery = $form->save();
            } catch (Doctrine_Validator_Exception $e) {

                $errorStack = $form->getObject()->getErrorStack();

                $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                foreach ($errorStack as $field => $errors) {
                    $message .= "$field (" . implode(", ", $errors) . "), ";
                }
                $message = trim($message, ', ');

                $this->getUser()->setFlash('error', $message);
                return sfView::SUCCESS;
            }

            $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $gallery)));
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }
}
