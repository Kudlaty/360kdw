<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
    <?php if($form->isNew()) { ?>
        <?php include_partial('gallery/new_form_fields', array('form' => $form)) ?>
        <div id="select_images">
            <div class="content"><input type="file" id="images_to_upload" name="gallery[image][]" multiple="multiple"></div>
        </div>
    <?php } ?>
  <?php echo form_tag_for($form, '@gallery') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php if($form->isNew()) { ?>
        <div>
            <div id="my_new_images"></div>
        </div>
    <?php } else { ?>
        <?php foreach ($configuration->getFormFields($form, 'edit') as $fieldset => $fields): ?>
            <?php include_partial('gallery/form_fieldset', array('gallery' => $gallery, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
        <?php endforeach; ?>
    <?php } ?>

    <?php include_partial('gallery/form_actions', array('gallery' => $gallery, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
