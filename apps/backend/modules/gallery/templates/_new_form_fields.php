<?php use_javascript('backend/gallery.js') ?>
<?php use_stylesheet('backend/gallery.css') ?>

<fieldset id="sf_fieldset_none" style="display: none; overflow:hidden;">
    <h2 style="text-align: right;"><a href="#" style="display: block; padding: 3px;" class="usun">x</a></h2>
    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_name">
        <div>
            <label for="gallery_name">Nazwa</label>
            <div class="content"><input type="text" id="gallery_name" name="gallery[name][]"></div>
        </div>
    </div>

    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_description">
        <div>
            <label for="gallery_description">Opis</label>
            <div class="content"><textarea id="gallery_description" name="gallery[description][]" cols="30" rows="4"></textarea></div>
        </div>
    </div>

    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_image">
        <div>
            <label for="gallery_image">Obraz</label>
            <div class="content gallery_image"></div>
        </div>
    </div>

    <div class="sf_admin_form_row sf_admin_foreignkey sf_admin_form_field_author_id">
        <div>
            <label for="gallery_author_id">Autor</label>
            <div class="content"><?php echo $form['author_id']->render() ?></div>
        </div>
    </div>

    <div class="sf_admin_form_row sf_admin_boolean sf_admin_form_field_status">
        <div>
            <label for="gallery_status">Status</label>
            <div class="content"><input type="checkbox" id="gallery_status" checked="checked" name="gallery[status][]"></div>
        </div>
    </div>
</fieldset>
