<?php

/**
 * news module configuration.
 *
 * @package    360kdw
 * @subpackage news
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsGeneratorConfiguration extends BaseNewsGeneratorConfiguration
{
}
