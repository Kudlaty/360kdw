<?php

require_once dirname(__FILE__).'/../lib/newsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/newsGeneratorHelper.class.php';

/**
 * news actions.
 *
 * @package    360kdw
 * @subpackage news
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends autoNewsActions
{
    public function executeBatchChange_status(sfWebRequest $request)
    {
        $ids = $request->getParameter('ids');

        $q = Doctrine_Query::create()
        ->from('News n')
        ->whereIn('n.id', $ids);

        foreach ($q->execute() as $item)
        {
            if($item->getStatus()) {
                $item->setStatus(false);
            } else {
                $item->setStatus(true);
            }
            $item->save();
        }

        $this->getUser()->setFlash('notice', "Zaznaczone newsy, zmieniły status");

        $this->redirect('news');
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->form = $this->configuration->getForm();
        $this->news = $this->form->getObject();

        $this->processMyForm($request, $this->form);

        $this->setTemplate('new');
    }

    protected function processMyForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

            try {
                $news = $form->save();

                $config = Doctrine_Core::getTable('Config')->findOneById(1);

                if($config->getSendMailsOnAddNews()) {
                    $mails = Doctrine_Core::getTable('NewsletterMail')->getAllActiveMails();

                    sfApplicationConfiguration::getActive()->loadHelpers(array('Url'));

                    $changers = array(
                        '{NEWS_LINK}' => str_replace(array('/backend_dev.php', '/backend.php'), '', url_for('news_show', $news, true)),
                        '{NEWS_NAME}' => $news->getName(),
                        '{NEWS_LEAD}' => $news->getLead(),
                    );

                    $mail_subject = $config->getEmailNewNewsSubject();
                    $mail_from = $config->getEmailNewNewsFrom();
                    $mail_body = $config->getEmailNewNewsBody();

                    $mail_body = str_replace(array_keys($changers), array_values($changers), $mail_body);


                    foreach($mails as $val) {
                        $mail = $this->getMailer()->compose();

                        $mail->setSubject($mail_subject);
                        $mail->setTo($val->getEmail());
                        $mail->setFrom($mail_from);
                        $mail->setBody($mail_body);

                        // send the email
                        $this->getMailer()->send($mail);
                    }
                }

            } catch (Doctrine_Validator_Exception $e) {
                $errorStack = $form->getObject()->getErrorStack();

                $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                foreach ($errorStack as $field => $errors) {
                    $message .= "$field (" . implode(", ", $errors) . "), ";
                }
                $message = trim($message, ', ');

                $this->getUser()->setFlash('error', $message);
                return sfView::SUCCESS;
            }

            $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $news)));

            if ($request->hasParameter('_save_and_add'))
            {
                $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

                $this->redirect('@news_new');
            }
            else
            {
                $this->getUser()->setFlash('notice', $notice);

                $this->redirect(array('sf_route' => 'news_edit', 'sf_subject' => $news));
            }
        }
        else
        {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }
}
