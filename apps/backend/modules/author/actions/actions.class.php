<?php

require_once dirname(__FILE__).'/../lib/authorGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/authorGeneratorHelper.class.php';

/**
 * author actions.
 *
 * @package    360kdw
 * @subpackage author
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class authorActions extends autoAuthorActions
{
    public function executeBatchChange_status(sfWebRequest $request)
    {
        $ids = $request->getParameter('ids');

        $q = Doctrine_Query::create()
        ->from('Author n')
        ->whereIn('n.id', $ids);

        foreach ($q->execute() as $item)
        {
            if($item->getStatus()) {
                $item->setStatus(false);
            } else {
                $item->setStatus(true);
            }
            $item->save();
        }

        $this->getUser()->setFlash('notice', "Zaznaczeni autorzy, zmienili status");

        $this->redirect('author');
    }

}
