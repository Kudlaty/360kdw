<?php

/**
 * people actions.
 *
 * @package    360kdw
 * @subpackage people
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class peopleActions extends sfActions
{
    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
        $people = Doctrine_Core::getTable('Author')->getPeople();

        $this->authors = $people;
    }
}
