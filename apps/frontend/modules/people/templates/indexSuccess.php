<?php use_helper('Date') ?>

<div class="txt_bg_podstrona">
    <div class="cl"></div>
    <div class="separator"></div>

    <div class="galeria_main_photo">
        <?php foreach($authors as $author) { ?>
        <div class="adds">
            <div class="autor_galeria">
                <img src="<?php echo $author->getThumbnailPath(300, 186) ?>" alt="<?php echo $author->getRawValue()->getName() ?>" />
            </div>
            <div class="first">
                <img src="/images/galeria/photo_icon.png" />
                <?php echo $author->getRawValue()->getName() ?>
            </div>
            <div class="second">
                <img src="/images/galeria/druzyna_icon.png" />
                drużyna: <?php echo $author->getRawValue()->getTeam() ?>
            </div>
            <div class="first">
                <img src="/images/galeria/www_icon.png" />
                dołączył(a): <?php echo format_date($author->getJoinDate(), 'dd MMMM yyyy') ?>
            </div>
        </div>
        <?php } ?>

        <div class="cl"></div>
    </div>
</div>