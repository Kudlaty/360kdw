<h1>Gallery item</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Name</th>
      <th>Description</th>
      <th>Author</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $gallery->getId() ?></td>
      <td><?php echo $gallery->getName() ?></td>
      <td><?php echo $gallery->getDescription() ?></td>
      <td><?php echo $gallery->getAuthorId() ?></td>
      <td><?php echo $gallery->getStatus() ?></td>
    </tr>
  </tbody>
</table>

