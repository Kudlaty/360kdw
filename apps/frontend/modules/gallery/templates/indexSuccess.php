<?php use_javascript('gallery.js') ?>


<div class="slider_in_artykul"></div>

<div class="txt_bg_podstrona">
    <div class="galeria_main_photo">
        <div class="galeria_arrow_l">
            <img src="<?php echo url_for('@images') ?>/galeria/l.png" />
        </div>

        <div class="main_photo_wrapper">
            <div class="main_photo">
            <?php foreach($gallerys as $key => $gallery) { ?>
                <div class="photo" data="<?php echo $gallery->getId() ?>">
                    <img src="<?php echo $gallery->getThumbnailPath(850) ?>" alt="<?php echo $gallery->getRawValue()->getName() ?>" />

                    <div style="clear: both;">
                        <div class="galeria_info">
                            <div class="galeria_header">
                                <?php echo $gallery->getRawValue()->getName() ?>
                            </div>
                            <div class="galeria_data">
                                <?php echo $gallery->getRawValue()->getDescription() ?>
                            </div>
                        </div>

                        <div class="galeria_social">
                            <?php include_partial('common/socials', array('link' => 'link'.$gallery->getId())) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>

        <div class="galeria_arrow_r">
            <img src="<?php echo url_for('@images') ?>/galeria/r.png" />
        </div>

        <div class="cl"></div>
    </div>


    <div class="galeria_list_photo">
    <?php foreach($gallerys as $key => $gallery) { ?>
        <div class="galeria_mini">
            <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
                <img data="<?php echo $gallery->getId() ?>" src="<?php echo $gallery->getThumbnailPath(0, 103) ?>" class="greyScale img_grayscale" />
                <img data="<?php echo $gallery->getId() ?>" src="<?php echo $gallery->getGrayThumbnailPath(0, 103) ?>" class="greyScale" />
            </div>
        </div>
    <?php } ?>
    </div>

    <div class="right_side">
        <?php include_component('common', 'Authors') ?>
    </div>

    <div class="cl"></div>
</div>
