<div class="comment">
    <?php include_partial("comment/comment_author", array('website' => $obj->getWebsite(), 'name' => $obj->getAuthor(), 'date' => $obj->getCreatedAt(), 'obj' => $obj, 'form_name' => $form_name, 'i' => $i)) ?>

    <?php include_partial("comment/comment_body", array('obj' => $obj)) ?>

    <div class="cl"></div>
</div>
