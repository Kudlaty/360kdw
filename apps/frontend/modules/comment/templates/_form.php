<div class="add_comment">
    <div class="error_wrap author_name_error"><div class="error_popup">Proszę podać nazwę</div></div>
    <div class="form_comment_icon"><img src="<?php echo url_for('@images') ?>/user.png" /></div>
    <input id="comment_author_name" type="text" name="<?php echo $form->getName() ?>[author_name]" class="form_comment" placeholder="Imię" required="required" />
    <div class="cl"></div>

    <div class="error_wrap author_email_error"><div class="error_popup">Proszę podać prawidłowy adres email</div></div>
    <div class="form_comment_icon"><img src="<?php echo url_for('@images') ?>/meil.png" /></div>
    <input id="comment_author_email" type="email" name="<?php echo $form->getName() ?>[author_email]" class="form_comment" placeholder="Twój email" required="required" />
    <div class="cl"></div>

    <div class="error_wrap author_www_error"><div class="error_popup">Proszę podać prawidłowy adres www</div></div>
    <div class="form_comment_icon"><img src="<?php echo url_for('@images') ?>/www.png" /></div>
    <input id="comment_author_www" type="text" name="<?php echo $form->getName() ?>[author_website]" class="form_comment" placeholder="Twója strona www"/>
    <div class="cl"></div>
    <div class="reply_author_div" id="reply_author_<?php echo $form->getName() ?>">
        <div class="form_comment_icon"><img src="<?php echo url_for('@images') ?>/user.png" /></div>
        <input id="<?php echo $form->getName() ?>_reply_author" type="text" name="<?php echo $form->getName() ?>[reply_author]" class="form_comment" value="" readonly="readonly" />
        <div class="cl"></div>
    </div>
</div>

<div class="textarea_container">
    <div class="error_wrap body_error"><div class="error_popup_textarea">Minimalna ilość znaków to: 10.</div></div>
    <textarea id="comment_body" name="<?php echo $form->getName() ?>[body]" class="comment_txt_area"></textarea>
    <div class="cl"></div>
</div>

<div class="reply_author_delete">
    <?php echo link_to_function("Usuń odpowiedź", "deleteReply('".$form->getName()."')", array('class' => 'delete_reply', 'id' => "reply_author_delete_".$form->getName()))."\n" ?>

    <input type="submit" value="<?php echo __('send', array(), 'vjComment') ?>" class="submit button_comment" />
    <div class="cl"></div>
</div>

<?php echo $form->renderHiddenFields() ?>

<div class="cl"></div>
