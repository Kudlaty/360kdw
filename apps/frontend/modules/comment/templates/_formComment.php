<?php use_helper('I18N', 'JavascriptBase') ?>

<?php use_javascript("/vjCommentPlugin/js/reply.js") ?>

<a name="comments-<?php echo $crypt ?>"></a>
<div class="form-comment">
<?php if( vjComment::checkAccessToForm($sf_user) ): ?>
  <form action="<?php echo url_for($sf_request->getUri()) ?>" method="post" name="<?php echo $form->getName() ?>">
    <?php include_partial("comment/form", array('form' => $form)) ?>
  </form>
  <div class="cl"></div>
<?php else: ?>
  <div id="notlogged"><?php echo __('Please log in to comment', array(), 'vjComment') ?></div>
<?php endif ?>
</div>
