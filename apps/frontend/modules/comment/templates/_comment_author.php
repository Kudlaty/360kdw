<div class="user_comment">
    <?php if($website != ""): ?>
        <a href="<?php echo $website ?>" target="_blank" rel="me nofollow"><?php echo $name ?></a><br />
    <?php else: ?>
        <?php echo $name ?><br />
    <?php endif; ?>

    <span>
        <?php echo format_date($date, "dd MMMM yyyy<br />H:mm") ?>
    </span>
    <br/><br/>

    <?php echo link_to_function('odpowiedz &raquo;',
            "reply('".$obj->getId()."','".$obj->getAuthor()."', '".$form_name."')",
            array('title' => 'Odpowiedz na ten komentarz')) ?>
    <?php /*echo link_to_function(
        image_tag('/vjCommentPlugin/images/error.png', array( 'alt' => 'zgłoś' )) ,
        'window.open(
            \''.url_for('@comment_reporting?id='.$obj->getId().'&num='.$i).'\',
            \'Dodaj nowy komentarz\',
            "menubar=no, status=no, scrollbars=no, menubar=no, width=565, height=300")',
        array('target' => '_blank', 'title' => 'Zgłoś komentarz - nowe okno' ))*/ ?>
</div>
