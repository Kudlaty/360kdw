<?php if($has_comments): ?>
<?php use_helper('Date', 'JavascriptBase', 'I18N') ?>
<?php if(commentTools::isGravatarAvailable()): ?>
<?php use_helper('Gravatar') ?>
<?php endif ?>

<?php if ($pager->haveToPaginate()): ?>
<?php include_partial('comment/pagination', array('pager' => $pager, 'route' => $sf_request->getUri(), 'crypt' => $crypt, 'position' => 'top')) ?>
<?php endif ?>

<?php foreach($pager->getResults() as $c): ?>
<?php include_partial("comment/comment", array('obj' => $c, 'i' => (++$i + $cpt), 'first_line' => ($i == 1), 'form_name' => $form_name)) ?>
<?php endforeach; ?>

<?php if ($pager->haveToPaginate()): ?>
<?php include_partial('comment/pagination', array('pager' => $pager, 'route' => $sf_request->getUri(), 'crypt' => $crypt, 'position' => 'back')) ?>
<?php else: ?>
<?php /*include_partial('comment/back_to_top', array('route' => $sf_request->getUri(), 'crypt' => $crypt, 'text' => true)) */?>
<?php endif ?>
<?php endif ?>
