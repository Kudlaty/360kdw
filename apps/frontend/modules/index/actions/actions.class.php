<?php

/**
 * index actions.
 *
 * @package    360kdw
 * @subpackage index
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class indexActions extends sfActions
{
    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request) {
        $this->four_news = Doctrine_Core::getTable('News')->getLastNews(4);
    }
}
