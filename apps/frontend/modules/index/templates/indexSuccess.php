<?php use_helper('Date') ?>

<div class="slider_in">
    <div class="box_info">
        <h2>Witaj na oficjalnej stronie 360 KDW Sokół!</h2>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis volutpat venenatis. Nullam ante diam, condimentum nec elementum bibendum, volutpat eu lorem.
        <button type="button" class="button_slider">DOŁĄCZ DO NAS</button>
    </div>

    <div class="cl"></div>
</div>

<div class="txt_bg">
    <div class="box_zajawka">
        <div class="header_zajwka">
                SZKOLENIE
        </div>

        <img src="<?php echo url_for('@images') ?>/zajawka_1.jpg" />
    </div>

    <div class="box_zajawka">
        <div class="header_zajwka">
                SZKOLENIE
        </div>

        <img src="<?php echo url_for('@images') ?>/zajawka_2.jpg" />
    </div>

    <div class="box_zajawka">
        <div class="header_zajwka">
                SZKOLENIE
        </div>

        <img src="<?php echo url_for('@images') ?>/zajawka_3.jpg" />
    </div>

    <div class="cl"></div>
</div>

<div class="news">
    <h1>News</h1>

    <div class="news_container">
    <?php foreach($four_news as $key => $news) { ?>
        <div class="news_box" <?php echo fmod($key, 2) ? 'style="float: right;"' : '' ?>>
            <div class="news_box_header_data">
                <span><?php echo format_date($news->getCreatedAt(), 'dd') ?></span>

                <div class="data">
                    <?php echo format_date($news->getCreatedAt(), 'MMM') ?><br />
                    <?php echo format_date($news->getCreatedAt(), 't') ?>
                </div>
            </div>

            <div class="news_box_header">
                <?php echo $news->getRawValue()->getName() ?>
            </div>

            <div class="cl"></div>

            <div class="news_box_txt">
                <?php echo $news->getRawValue()->getLead() ?>
                <br />
                <a href="<?php echo url_for('news_show', $news) ?>" title="Więcej o <?php echo $news->getRawValue()->getName() ?>">więcej ></a>
            </div>
        </div>

        <?php if(fmod($key, 2) || $key == (count($four_news)-1)) { ?>
            <div class="cl"></div>
        <?php } ?>
    <?php } ?>
    </div>

    <div class="archive">
        <a href="<?php echo url_for('@news') ?>" title="News">starsze wiadomości ></a>
    </div>
</div>

<div class="right_side">
    <?php include_component('common', "Newsletter") ?>

    <?php include_component('common', "LastEvents") ?>
</div>
<div class="cl"></div>
