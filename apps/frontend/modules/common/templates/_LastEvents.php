<?php if(count($lastEvents)) { ?>

<?php use_helper('Date') ?>
<?php use_javascript('lastEvents.js') ?>

<div class="event_box">
    <h1>Co nas czeka?</h1>

    <?php foreach($lastEvents as $key => $event) { $class = fmod($key, 2) ? 'first' : 'second'; ?>
    <div class="<?php echo $class ?>">
        <div class="list">
            <img src="<?php echo url_for('@images') ?>/marker.png"/>
            <?php echo $event->getRawValue()->getName() ?> - <?php echo format_date($event->getEventDate(), 'MM.dd.yyyy') ?>
            <?php if($event->getEventTime()) { ?>
                <?php echo format_date($event->getEventTime(), 'H:mm') ?>
            <?php } ?>
        </div>
        <div class="details">
            <img src="<?php echo url_for('@images') ?>/marker.png">
            <?php if(trim($event->getRawValue()->getTopic()) == "") { ?>
                <?php echo $event->getRawValue()->getName() ?>
            <?php } else { ?>
                <?php echo $event->getRawValue()->getTopic() ?>
            <?php } ?>
            <br />
            <br />
            Odpowiedzialny:
            <span><?php echo $event->getRawValue()->getResponsible() ?></span>
            <br>
            Kiedy:
            <span><?php echo format_date($event->getEventDate(), 'MM.dd.yyyy') ?><?php if($event->getEventTime()) { ?>, <?php echo format_date($event->getEventTime(), 'H:mm') ?>
            <?php } ?></span>
            <br />
            Gdzie:
            <span><?php echo $event->getRawValue()->getWhere() ?></span>
            <br />
            <div class="cl"></div>
        </div>
    </div>
    <?php } ?>
</div>
<?php } ?>