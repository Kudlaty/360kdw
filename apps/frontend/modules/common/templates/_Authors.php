<?php if(count($authors)) { ?>

<h1 style="margin-top:0px;">Autorzy zdjęć</h1>

<?php foreach($authors as $author) { ?>
<div class="author_box">
    <div class="autor_galeria">
        <img src="<?php echo $author->getThumbnailPath(300, 186) ?>" alt="<?php echo $author->getRawValue()->getName() ?>" />
    </div>
    <div class="first">
        <img src="<?php echo url_for('@images') ?>/galeria/photo_icon.png" />
        autor: <?php echo $author->getRawValue()->getName() ?>
    </div>
    <div class="second">
        <img src="<?php echo url_for('@images') ?>/galeria/druzyna_icon.png" />
        drużyna: <?php echo $author->getRawValue()->getTeam() ?>
    </div>
    <div class="first">
        <img src="<?php echo url_for('@images') ?>/galeria/www_icon.png" />
        <a href="<?php echo $author->getRawValue()->getWww() ?>" rel="nofollow">strona www/portfolio &raquo;</a>
    </div>
</div>
<?php } ?>
<?php } ?>