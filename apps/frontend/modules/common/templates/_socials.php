<?php use_javascript('social.js') ?>

<div class="fb">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <a href="http://www.facebook.com/sharer.php?u=<?php echo $sf_request->getUri() ?>">
            <img src="<?php echo url_for('@images') ?>/fb_kolor.jpg" class="greyScale img_grayscale" alt="Facebook" />
            <img src="<?php echo url_for('@images') ?>/fb.jpg" class="greyScale" alt="Facebook" />
        </a>
    </div>
</div>
<div class="gplus">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <a href="https://plus.google.com/share?url=<?php echo $sf_request->getUri() ?>">
            <img src="<?php echo url_for('@images') ?>/gplus_kolor.jpg" class="greyScale img_grayscale" alt="G+" />
            <img src="<?php echo url_for('@images') ?>/gplus.jpg" class="greyScale" alt="G+" />
        </a>
    </div>
</div>
<div class="gmail">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/gmail_kolor.jpg" class="greyScale img_grayscale" alt="GMail" />
        <img src="<?php echo url_for('@images') ?>/gmail.jpg" class="greyScale" alt="GMail" />
    </div>
</div>
<div class="tw">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <a href="http://twitter.com/home?status=<?php echo $sf_request->getUri() ?>">
            <img src="<?php echo url_for('@images') ?>/tw_kolor.jpg" class="greyScale img_grayscale" alt="Twitter" />
            <img src="<?php echo url_for('@images') ?>/tw.jpg" class="greyScale" alt="Twitter" />
        </a>
    </div>
</div>
<div class="tumblr">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/tumblr_kolor.jpg" class="greyScale img_grayscale" alt="Tumblr" />
        <img src="<?php echo url_for('@images') ?>/tumblr.jpg" class="greyScale" alt="Tumblr" />
    </div>
</div>
<div class="yt">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/yt_kolor.jpg" class="greyScale img_grayscale" alt="YouTube" />
        <img src="<?php echo url_for('@images') ?>/yt.jpg" class="greyScale" alt="YouTube" />
    </div>
</div>
<div class="be">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/be_kolor.jpg" class="greyScale img_grayscale" alt="be" />
        <img src="<?php echo url_for('@images') ?>/be.jpg" class="greyScale" alt="be" />
    </div>
</div>
<div class="bbb">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/bbb_kolor.jpg" class="greyScale img_grayscale" alt="bbb" />
        <img src="<?php echo url_for('@images') ?>/bbb.jpg" class="greyScale" alt="bbb" />
    </div>
</div>
<div class="p">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/p_kolor.jpg" class="greyScale img_grayscale" alt="p" />
        <img src="<?php echo url_for('@images') ?>/p.jpg" class="greyScale" alt="p" />
    </div>
</div>
<div class="drop">
    <div style="display: inline-block; width: 29px; height: 29px;" class="img_wrapper">
        <img src="<?php echo url_for('@images') ?>/drop_kolor.jpg" class="greyScale img_grayscale" alt="Dropbox" />
        <img src="<?php echo url_for('@images') ?>/drop.jpg" class="greyScale" alt="Dropbox" />
    </div>
</div>
