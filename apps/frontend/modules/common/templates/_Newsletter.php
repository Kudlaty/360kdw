<?php use_javascript('newsletter.js') ?>

<div class="newsletter_box">
    <h1>Newsletter</h1>

    <?php if($sf_user->hasFlash('mail_saved')) { ?>
        <div class="mail_saved">
            <?php echo $sf_user->getFlash('mail_saved'); ?>
        </div>
    <?php } ?>

    <div class="newsletter">
        Lorem ipsum dolor sit amet,Lorem ipsum
        dolor sit amet. Lorem ipsum dolor sit
        amet,Lorem ipsum dolor sit amet.
        <form id="newsletter_form" method="post" action="<?php echo str_replace('/news/', '/frontend_dev.php/news/', $sf_request->getUri()); ?>">
            <img src="<?php echo url_for('@images') ?>/letter.png" />
            <div class="newsletter_email_error">
                <div class="newsletter_error">Podaj prawidłowy email</div>
            </div>
            <input type="email" name="newsletter_email" class="form_newsletter" placeholder="Twój email"/>

            <input type="submit" class="button_newsletter" value="zapisz się" />
        </form>
    </div>

    <div class="newsletter_img">
        <img src="<?php echo url_for('@images') ?>/pasek_newsletter.jpg" />
    </div>

    <div class="cl"></div>
</div>