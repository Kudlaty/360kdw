<?php

/**
 * common actions.
 *
 * @package    360kdw
 * @subpackage common
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class commonActions extends sfActions
{
    /**
    * Executes common action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }
}
