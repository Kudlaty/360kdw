<?php

/**
 * common components.
 *
 * @package    360kdw
 * @subpackage common
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: components.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class commonComponents extends sfComponents {
    public function executeAuthors(sfWebRequest $request) {
        $limit = 2;
        if(isset($this->limit)) {
            $limit = $this->limit;
        }

        $this->authors = Doctrine_Core::getTable('Author')->getRandomAuthors($limit);
    }

    public function executeLastEvents(sfWebRequest $request) {
        $limit = 5;
        if(isset($this->limit)) {
            $limit = $this->limit;
        }

        $this->lastEvents = Doctrine_Core::getTable('Event')->getLastEvent($limit);
    }

    public function executeNewsletter(sfWebRequest $request) {
        if($request->isMethod('post') && $request->hasParameter('newsletter_email')) {
            $this->form = new NewsletterMailForm();

            $formValues = array();
            $formValues['id'] = "";
            $formValues['email'] = $request->getParameter('newsletter_email');
            $formValues['status'] = 1;

            $mail = Doctrine_Core::getTable('NewsletterMail')->findOneByEmail($formValues['email']);

            if(!$mail) {
                $this->form->bind( $formValues );
                if ($this->form->isValid()) {
                    $this->form->save();
                }
            } elseif(!$mail->getStatus()) {
                $mail->setStatus(True);
                $mail->save();
            }

            $url = $request->getUri();

            $this->getUser()->setFlash( 'mail_saved', 'Mail został zapisany do bazy newslettera.');
            $this->getContext()->getController()->redirect($url, 0, 302);
        }
    }
}
