<h1>Newss List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Name</th>
      <th>Lead</th>
      <th>Description</th>
      <th>Image</th>
      <th>Status</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($newss as $news): ?>
    <tr>
      <td><?php echo $news->getId() ?></td>
      <td><?php echo $news->getName() ?></td>
      <td><?php echo $news->getLead() ?></td>
      <td><?php echo $news->getDescription() ?></td>
      <td><?php echo $news->getImage() ?></td>
      <td><?php echo $news->getStatus() ?></td>
      <td><?php echo $news->getCreatedAt() ?></td>
      <td><?php echo $news->getUpdatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
