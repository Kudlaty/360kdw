<?php use_helper('Date') ?>

<div class="slider_in_artykul"></div>

<div class="txt_bg_podstrona">
    <div class="artykul">
        <div class="artykul_header">
            <?php echo $news->getRawValue()->getName() ?>
        </div>

        <div class="artykul_data">
            <?php echo format_date($news->getCreatedAt(), 'f')  ?>
        </div>

        <div class="artykul_txt">
            <strong><?php echo $news->getRawValue()->getLead() ?></strong>

            <img src="<?php echo $news->getThumbnailPath(600); ?>" />

            <?php echo $news->getRawValue()->getDescription() ?>
        </div>

        <div class="artykul_social">
            <?php include_partial('common/socials', array('link' => 'link')) ?>

            <button type="button" class="button_comment">dodaj komentarz</button>

            <div class="cl"></div>
        </div>

        <?php include_component('comment', 'formComment', array('object' => $news)) ?>
        <?php include_component('comment', 'list', array('object' => $news, 'i' => 0)) ?>
    </div>


    <div class="right_side">
        <?php include_component('common', "Newsletter") ?>

        <?php include_component('common', "LastEvents") ?>
    </div>
<div class="cl"></div>
</div>
