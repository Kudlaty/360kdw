<?php

/**
 * news actions.
 *
 * @package    360kdw
 * @subpackage news
 * @author     Karol Fuksiewicz
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $query = NewsTable::getQuery();
        $this->newss = $query->execute();
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->news = Doctrine_Core::getTable('News')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->news);
    }

    public function executeNewsImage(sfWebRequest $request)
    {
        $id = $request->getParameter('id');
        $image = Doctrine_Core::getTable('News')->find(array($id));
        $this->forward404Unless($image);

        $file = $image->getImage();

        $upDir = sfConfig::get('sf_upload_dir');
        $img_dir = $upDir.DIRECTORY_SEPARATOR.'news'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR;
        $filename = $img_dir.$file;

        $resize = $request->getParameter('resize', 0);
        $aspect = $request->getParameter('aspect', 1);
        $height = $request->getParameter('height', 0);
        $width = $request->getParameter('width', 0);

        $thumb_dir = $upDir.DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.'news'.DIRECTORY_SEPARATOR.$width.DIRECTORY_SEPARATOR.$height.DIRECTORY_SEPARATOR.$id;

        $thumbnail = kdw::resize($file, $filename, $height, $width, $thumb_dir, $aspect, $resize, Null);

        $response = $this->getResponse();
        $response->setHttpHeader('Cache-Control', 'max-age=7776000, public, must-revalidate', true); // 3 month 3*30*24*3600
        $response->setContentType($thumbnail->getMIMEType());
        $response->setContent($thumbnail);

        return sfView::NONE;
    }
}
