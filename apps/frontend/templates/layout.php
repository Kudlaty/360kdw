<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title>
        <?php if (!include_slot('title')): ?>
            360 KDW Sokół
        <?php endif; ?>
    </title>

    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="stylesheet" href="<?php echo url_for('@css') ?>/jquery-ui-1.8.23.css" type="text/css" />
    <?php include_stylesheets() ?>
    <script type="text/javascript" src="<?php echo url_for('@js') ?>/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="<?php echo url_for('@js') ?>/jquery-ui-1.8.23.min.js"></script>
    <?php include_javascripts() ?>

    <?php if (has_slot('script')): ?>
        <?php include_slot('script') ?>
    <?php endif; ?>
</head>
<body>
    <div class="wrapper">
        <?php include_partial('index/slider') ?>

        <div class="wrap">
            <div class="header">
                <div class="top_header">
                    <div class="bg_logo">
                        <a href="<?php echo url_for('@homepage') ?>"><img src="<?php echo url_for('@images') ?>/logo.png" /></a>
                    </div>
                    <div class="menu">
                        <ul>
                            <li><a href="<?php echo url_for('@people') ?>" title="Ludzie">LUDZIE</a></li>
                            <li><a href="<?php echo url_for('@meeting') ?>" title="Spotkania">SPOTKANIA</a></li>
                            <li><a href="<?php echo url_for('@travel') ?>" title="Wyprawy">WYPRAWY</a></li>
                            <li><a href="<?php echo url_for('@gallery') ?>" title="Galeria">GALERIA</a></li>
                            <li><a href="<?php echo url_for('@contact') ?>" title="Kontakt">KONTAKT</a></li>
                        </ul>
                    </div>
                    <div class="cl"></div>
                </div>
            </div>

            <div id="container">
                <?php echo $sf_content ?>
            </div>
        </div>
    </div>

    <div class="footer"></div>
</body>
</html>
